package service

import (
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"

	"github.com/mitchellh/go-ps"
	"gitlab.com/sirenia/open/ahab/checkers"
	"gitlab.com/sirenia/open/ahab/downloaders"
)

type State int

const (
	Running = iota
	Updating
	Exiting
	RequirementNotMet
	Erroring
	None
)

type ServiceOptions struct {
	NoRequirements bool
}

type Service struct {
	Name         string        `yaml:"name"`
	Updates      *Update       `yaml:"updates"`
	Requirements []Requirement `yaml:"requirements,omitempty"`
	Execute      *Executable   `yaml:"execute"`
	Status       *Status       `yaml:"status"`
	Restart      bool          `yaml:"restart"`
	Cmd          *exec.Cmd
	stopping     bool
}

type Error struct {
	Who   *Service
	Error error
}

type UpdateMessage struct {
	Who     *Service
	State   State
	Message string
}

func (s *Service) checkForUpdates(currentVersion *string, updates chan UpdateMessage) (*string, error) {
	if s.Updates.Checker != nil {
		updates <- UpdateMessage{
			Who:     s,
			State:   Updating,
			Message: "Checking for updates ..."}

		update, err := s.Updates.Checker.Check(currentVersion)
		if err != nil {
			updates <- UpdateMessage{
				Who:     s,
				State:   Updating,
				Message: fmt.Sprintf("Error checking for update - %v.", err)}
			return nil, err
		}
		if update == nil {
			updates <- UpdateMessage{
				Who:     s,
				State:   Updating,
				Message: "No updates available."}
			<-time.After(time.Second)
			return nil, nil
		}
		return update, nil
	}
	return nil, nil
}

func (s *Service) downloadUpdate(version string, updates chan UpdateMessage) error {

	if s.Updates.Downloader != nil {
		updates <- UpdateMessage{
			Who:     s,
			State:   Updating,
			Message: fmt.Sprintf("Update %v available. Starting download ... ", version)}
		<-time.After(time.Second * 1)

		if s.Updates.Downloader != nil {
			// Progress updates
			progress := make(chan int)
			go func() {
				for {
					p := <-progress
					updates <- UpdateMessage{
						Who:     s,
						State:   Updating,
						Message: fmt.Sprintf("Downloading %v ... %v%%", version, p)}
					if p >= 100 {
						updates <- UpdateMessage{
							Who:     s,
							State:   Updating,
							Message: "Download complete. Unpacking ..."}
						<-time.After(time.Second)
						break
					}
				}
			}()
			dir := s.Name + string(filepath.Separator) + version
			err := s.Updates.Downloader.Download(s.Name, version, dir, progress)
			if err != nil {
				updates <- UpdateMessage{
					Who:     s,
					State:   Erroring,
					Message: fmt.Sprintf("Error downloading update - %v.", err)}
			}

			// delete old versions
			files, err := ioutil.ReadDir(s.Name)
			if err != nil {
				return err
			}
			for _, file := range files {
				if file.Name() != version {
					if err := os.RemoveAll(s.Name + string(filepath.Separator) + file.Name()); err != nil {
						return err
					}
				}
			}
		}

	} else {
		updates <- UpdateMessage{
			Who:     s,
			State:   Updating,
			Message: fmt.Sprintf("Update %v available. Manual download required.", version)}
	}
	return nil
}

func (s *Service) Run(updates chan UpdateMessage, errors chan Error, opts ServiceOptions) {
	running, err := s.IsRunning()
	if err != nil {
		errors <- Error{Who: s, Error: err}
		return
	}
	if running {
		errors <- Error{Who: s, Error: fmt.Errorf("Looks like %v is already running. It must be killed before running this tool.", s.Name)}
		return
	}

	// Continuously check requirements and proceed when all are met
	if !opts.NoRequirements {
		for {
			if msg, err := s.CheckRequirements(); msg != nil || err != nil {
				if err != nil {
					errors <- Error{Who: s, Error: err}
				} else {
					updates <- UpdateMessage{
						Who:     s,
						State:   RequirementNotMet,
						Message: fmt.Sprintf("Requirement not met: %v", *msg)}
					<-time.After(time.Second * 5)
					//errors <- fmt.Errorf("Requirement not met: %v", *msg)
				}
				continue
			}
			break
		}
	}

	// Figure out current version if any
	currentVersion, err := s.CurrentVersion()
	if err != nil {
		errors <- Error{Who: s, Error: err}
	}

	// Check for updates
	newVersion, err := s.checkForUpdates(currentVersion, updates)
	if err != nil {
		errors <- Error{Who: s, Error: err}
	}

	// Download update if any
	if newVersion != nil {
		err = s.downloadUpdate(*newVersion, updates)
		if err != nil {
			errors <- Error{Who: s, Error: fmt.Errorf("Downloading %v failed with %v.", *newVersion, err)}
		}
		currentVersion = newVersion
	}

	if currentVersion == nil {
		errors <- Error{Who: s, Error: fmt.Errorf("Could not find a suitable version of %v to start.", s.Name)}
	}

	// TODO Create pipe for logs

	// Kick of process
	go func() {
		err := s.run(currentVersion, updates)
		if err != nil {
			errors <- Error{Who: s, Error: err}
		}
	}()
}

func (s *Service) run(currentVersion *string, updates chan UpdateMessage) error {

	for {
		// Update check completed. Start process.
		updates <- UpdateMessage{
			Who:     s,
			State:   None,
			Message: fmt.Sprintf("Starting %v.", s.Name)}
		// TODO path combine
		cmd := exec.Command(s.Name+string(filepath.Separator)+*currentVersion+string(filepath.Separator)+string(filepath.Separator)+s.Execute.Binary, s.Execute.Args...)
		for _, env := range s.Execute.Env {
			cmd.Env = append(os.Environ(), env)
		}
		<-time.After(time.Second * 5)
		s.Cmd = cmd
		if err := s.Cmd.Start(); err != nil {
			// TODO pipe output to file
			out, errOutput := s.Cmd.CombinedOutput()
			if errOutput != nil {
				out = []byte("Error getting output.")
			}
			fmt.Println(out)
			updates <- UpdateMessage{
				Who:     s,
				State:   Erroring,
				Message: err.Error()}
			return err
		}
		updates <- UpdateMessage{
			Who:     s,
			State:   Running,
			Message: "Running ..."}
		err := s.Cmd.Wait()
		updates <- UpdateMessage{
			Who:     s,
			State:   Exiting,
			Message: fmt.Sprintf("Exited.")}
		if !s.Restart {
			return err
		}
		updates <- UpdateMessage{
			Who:     s,
			State:   None,
			Message: "Restarting in 5 seconds."}
		<-time.After(time.Second * 5)
	}
	return nil
}

func (s *Service) CheckRequirements() (*string, error) {
	for _, r := range s.Requirements {
		switch {
		case r.Status.Process != "":
			p := r.Status.Process
			foundProcess := false
			processes, err := ps.Processes()
			if err != nil {
				return nil, err
			}
			for _, process := range processes {
				if strings.Contains(p, process.Executable()) {
					foundProcess = true
					break
				}
				if !foundProcess {
					return &r.Message, nil
				}
			}
			break
		case r.Status.Port != "":
			conn, err := net.DialTimeout("tcp", fmt.Sprintf("localhost:%v", r.Status.Port), time.Second*5)
			if conn != nil {
				_ = conn.Close()
			}
			if err != nil {
				return &r.Message, nil
			}
			break
		default:
			// TODO signal checking
			return nil, nil
		}
	}
	return nil, nil
}

func (s *Service) Stop() {
	s.stopping = true
	if s.Cmd != nil && s.Cmd.Process != nil {
		s.Cmd.Process.Kill()
	}
}

func (s *Service) IsRunning() (bool, error) {
	processes, err := ps.Processes()
	if err != nil {
		return false, err
	}

	for _, process := range processes {
		if strings.Contains(s.Name, process.Executable()) {
			return true, nil
		}
	}
	return false, nil
}

func exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}

func (s *Service) CurrentVersion() (*string, error) {
	dir := "." + string(filepath.Separator) + s.Name
	err := os.MkdirAll(dir, 0777)
	if err != nil {
		return nil, err
	}
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return nil, err
	}
	if len(files) == 0 {
		return nil, nil
	}
	if len(files) > 1 {
		return nil, fmt.Errorf("Multiple versions of %v available(%v).", s.Name, files)
	}
	if !files[0].IsDir() {
		return nil, fmt.Errorf("Need %v to contain a single dir.", s.Name)
	}
	name := files[0].Name()
	return &name, nil
}

type Executable struct {
	Binary string   `yaml:"binary"`
	Env    []string `yaml:"env"`
	Args   []string `yaml:"args"`
}

type Update struct {
	Checker    *checkers.CheckerBase       `yaml:"checker"`
	Downloader *downloaders.DownloaderBase `yaml:"downloader"`
}

type Status struct {
	Process string `yaml:"process,omitempty"`
	Port    string `yaml:"port,omitempty"`
	Signal  string `yaml:"signal,omitempty"`
}

type Requirement struct {
	Message string  `yaml:"message"`
	Status  *Status `yaml:"status"`
}
