package checkers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sort"

	"github.com/davecgh/go-spew/spew"
)

type GitlabApi struct {
	Token   string `yaml:"token"`
	Group   string `yaml:"group"`
	Project string `yaml:"project"`
}

type GitlabCommit struct {
	GitlabApi *GitlabApi `yaml:"gitlab-api"`
	Ref       string     `yaml:"ref"`
}

func (c *GitlabCommit) Check(installedVersion *string) (*string, error) {

	buildsURL := fmt.Sprintf("https://gitlab.com/api/v4/projects/%v/jobs?private_token=%v&scope=success&per_page=500", c.GitlabApi.Project, c.GitlabApi.Token)

	spew.Dump(buildsURL)
	resp, err := http.Get(buildsURL)
	if err != nil {
		return nil, err
	}
	decoder := json.NewDecoder(resp.Body)
	var allBuilds []Build
	if err := decoder.Decode(&allBuilds); err != nil {
		return nil, err
	}

	builds := []Build{}

	for _, build := range allBuilds {
		if build.Ref == c.Ref {
			builds = append(builds, build)
		}
	}

	sort.Sort(ByFinishedAt(builds))

	if len(builds) > 0 && (installedVersion == nil || builds[0].Commit.ID != *installedVersion) {
		return &(builds[0].Commit.ID), nil
	}

	// No updates.
	return nil, nil
}

type Commit struct {
	ID      string `json:"id"`
	Message string `json:"message"`
}

type Build struct {
	ID            int            `json:"id"`
	Name          string         `json:"name"`
	Commit        *Commit        `json:"commit"`
	FinishedAt    string         `json:"finished_at"`
	Ref           string         `json:"ref"`
	ArtifactsFile *ArtifactsFile `json:"artifacts_file"`
}

type ArtifactsFile struct {
	Filename string `json:"filename"`
	Size     int    `json:"size"`
}

type ByFinishedAt []Build

func (a ByFinishedAt) Len() int {
	return len(a)
}

func (a ByFinishedAt) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

func (a ByFinishedAt) Less(i, j int) bool {
	return a[i].FinishedAt > a[j].FinishedAt
}
