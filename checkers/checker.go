package checkers

type Checker interface {
	Check(currentVersion *string) (*string, error)
}

type CheckerBase struct {
	actual Checker
}

func (b *CheckerBase) Check(currentVersion *string) (*string, error) {
	return b.actual.Check(currentVersion)
}

func (b *CheckerBase) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var props map[string]interface{}
	err := unmarshal(&props)
	if err != nil {
		return err
	}

	if _, ok := props["gitlab-api"]; ok {
		var commit GitlabCommit
		err = unmarshal(&commit)
		if err != nil {
			return err
		}
		b.actual = &commit
		return nil
	}

	return nil
}
