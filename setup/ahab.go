package setup

import (
	"fmt"
	"os"
	"sync"
	"syscall"
	"time"

	"html/template"
	"os/signal"

	"github.com/gosuri/uilive"
	"gitlab.com/sirenia/open/ahab/about"
	"gitlab.com/sirenia/open/ahab/service"

	"github.com/fatih/color"
)

var (
	ui = `
{{ .Banner }}
  
  ahab {{ .Version }} {{ .Status }}

{{ range $index, $update := .ServiceUpdates }}
  [{{ $update.Who}}] {{ $update.Message }}
{{ end }}

Hit ctrl+c to quit.
`
	warning  = color.New(color.BgRed).Add(color.FgWhite).SprintFunc()
	running  = color.New(color.BgGreen).Add(color.FgWhite).SprintFunc()
	updating = color.New(color.FgMagenta).SprintFunc()
)

func New(conf *Configuration) *ahab {
	w := uilive.New()
	w.RefreshInterval = time.Millisecond * 200
	return &ahab{
		Banner:         conf.Banner,
		Version:        about.Version,
		Services:       conf.Services,
		Status:         "initializing ...",
		Writer:         w,
		Template:       nil,
		ServiceUpdates: make([]Update, len(conf.Services)),
		Mutex:          sync.Mutex{}}
}

type ahab struct {
	Banner         string
	Version        string
	Status         string
	Services       []service.Service
	Writer         *uilive.Writer
	Template       *template.Template
	ServiceUpdates []Update
	Mutex          sync.Mutex
}

type Update struct {
	Who     string
	Message string
}

func (c *ahab) Run(noRequirements bool) error {

	c.Status = "running ..."

	// Setup SIGTERM handling
	signals := make(chan os.Signal, 2)
	signal.Notify(signals, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-signals
		c.KillAll()
		<-time.After(time.Second * 2)
		os.Exit(1)
	}()

	tmpl, err := template.New("name").Parse(ui)
	if err != nil {
		return err
	}
	c.Template = tmpl

	updates := make([]chan service.UpdateMessage, len(c.Services))
	errors := make(chan service.Error)

	c.Writer.Start()

	for i, s := range c.Services {
		updates[i] = make(chan service.UpdateMessage)
		serv := s
		go serv.Run(updates[i], errors, service.ServiceOptions{NoRequirements: noRequirements})
		go func(srvc *service.Service, updates chan service.UpdateMessage, errors chan service.Error, i int) {
			for {
				select {
				case update := <-updates:
					var u Update
					switch update.State {
					case service.Updating:
						u = Update{Who: updating(update.Who.Name), Message: update.Message}
					case service.RequirementNotMet, service.Erroring, service.Exiting:
						u = Update{Who: warning(update.Who.Name), Message: update.Message}
					case service.Running:
						u = Update{Who: running(update.Who.Name), Message: update.Message}
					default:
						u = Update{Who: update.Who.Name, Message: update.Message}
					}
					c.ServiceUpdates[i] = u
					c.Render()
				}
			}
		}(&serv, updates[i], errors, i)
	}
	serviceError := <-errors
	fmt.Printf("Oh no! %v failed with: %v", serviceError.Who, serviceError.Error)
	c.KillAll()
	return nil
}

func (c *ahab) KillAll() {
	c.Status = warning("stopping all services ...")
	c.Render()
	for _, service := range c.Services {
		service.Stop()
	}
}

func (c *ahab) Render() {
	c.Mutex.Lock()
	defer c.Mutex.Unlock()

	c.Template.Execute(c.Writer, c)
	c.Writer.Wait()
}
