package setup

import "gitlab.com/sirenia/open/ahab/service"

type Configuration struct {
	Banner   string            `yaml:"banner"`
	Services []service.Service `yaml:"services"`
}
