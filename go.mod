module gitlab.com/sirenia/open/ahab

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/fatih/color v1.4.1
	github.com/fsnotify/fsnotify v0.0.0-20160816051541-f12c6236fe7b
	github.com/gosuri/uilive v0.0.0-20160202011846-efb88ccd0599
	github.com/hashicorp/hcl v0.0.0-20160916130100-ef8133da8cda
	github.com/inconshreveable/mousetrap v1.0.0
	github.com/kr/fs v0.0.0-20131111012553-2788f0dbd169
	github.com/magiconair/properties v0.0.0-20160908093658-0723e352fa35
	github.com/mattn/go-colorable v0.1.0 // indirect
	github.com/mattn/go-isatty v0.0.0-20170216235908-dda3de49cbfc
	github.com/mitchellh/go-ps v0.0.0-20160822165447-e2d21980687c
	github.com/mitchellh/mapstructure v0.0.0-20160808181253-ca63d7c062ee
	github.com/pelletier/go-buffruneio v0.1.0
	github.com/pelletier/go-toml v0.0.0-20160920070715-45932ad32dfd
	github.com/pkg/errors v0.0.0-20160916110212-a887431f7f6e
	github.com/pkg/sftp v0.0.0-20160908100035-8197a2e58073
	github.com/spf13/afero v0.0.0-20160919210114-52e4a6cfac46
	github.com/spf13/cast v0.0.0-20160919202641-60e7a69a428e
	github.com/spf13/cobra v0.0.0-20160830174925-9c28e4bbd74e
	github.com/spf13/jwalterweatherman v0.0.0-20160311093646-33c24e77fb80
	github.com/spf13/pflag v0.0.0-20160915153101-c7e63cf4530b
	github.com/spf13/viper v0.0.0-20160923232044-e26d6aedc153
	golang.org/x/crypto v0.0.0-20161028195300-b2fa06b6af4b
	golang.org/x/sys v0.0.0-20160916181909-8f0908ab3b24
	golang.org/x/text v0.0.0-20170113092929-f72d8390a633
	gopkg.in/yaml.v2 v2.0.0-20160715033755-e4d366fc3c79
)
