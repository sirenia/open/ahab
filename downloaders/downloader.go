package downloaders

type Downloader interface {
	Download(name, version, dest string, progress chan int) error
}

type DownloaderBase struct {
	actual Downloader
}

func (b *DownloaderBase) Download(name, version, dest string, progress chan int) error {
	return b.actual.Download(name, version, dest, progress)
}

func (b *DownloaderBase) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var props map[string]interface{}
	err := unmarshal(&props)
	if err != nil {
		return err
	}

	if _, ok := props["gitlab-api"]; ok {
		var artifact GitlabArtifact
		err = unmarshal(&artifact)
		if err != nil {
			return err
		}
		b.actual = &artifact
		return nil
	}

	return nil
}
