package downloaders

import (
	"archive/zip"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"

	"gitlab.com/sirenia/open/ahab/checkers"
)

type GitlabArtifact struct {
	GitlabApi *checkers.GitlabApi `yaml:"gitlab-api"`
	Job       string              `yaml:"job"`
	Ref       string              `yaml:"ref"`
}

// ProgressWriter counts the number of bytes written to it.
type ProgressWriter struct {
	Progress        chan int
	CurrentProgress int
	Current         int64 // Current # of bytes transferred
	Total           int64 // Total # of bytes to be transferred
}

// Write implements the io.Writer interface.
//
// Always completes and never returns an error.
func (wc *ProgressWriter) Write(p []byte) (int, error) {
	n := len(p)
	wc.Current += int64(n)
	newProgress := int((float64(wc.Current) / float64(wc.Total) * 100))
	if newProgress != wc.CurrentProgress {
		wc.CurrentProgress = newProgress
		wc.Progress <- wc.CurrentProgress
	}
	return n, nil
}

func (a *GitlabArtifact) Download(name, version, dest string, progress chan int) error {
	artifactURL := fmt.Sprintf("https://gitlab.com/api/v4/projects/%v/jobs/artifacts/%v/download?job=%v&private_token=%v&per_page=500", a.GitlabApi.Project, a.Ref, a.Job, a.GitlabApi.Token)
	resp, err := http.Get(artifactURL)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	var src io.Reader
	if progress != nil {
		src = io.TeeReader(resp.Body, &ProgressWriter{Progress: progress, Total: resp.ContentLength})
	} else {
		src = resp.Body
	}
	download, err := ioutil.ReadAll(src)
	if err != nil {
		return err
	}
	os.MkdirAll("."+string(filepath.Separator)+"tmp", 0777)
	tmpFile := "tmp" + string(filepath.Separator) + name + "_" + version
	err = ioutil.WriteFile(tmpFile, download, 0644)
	if err != nil {
		return err
	}
	// mkdir
	os.MkdirAll("."+string(filepath.Separator)+dest, 0777)
	if err := Unzip(tmpFile, dest); err != nil {
		return err
	}
	return nil
}

func Unzip(src, dest string) error {
	r, err := zip.OpenReader(src)
	if err != nil {
		return err
	}
	defer func() {
		if err := r.Close(); err != nil {
			panic(err)
		}
	}()

	os.MkdirAll(dest, 0755)

	// Closure to address file descriptors issue with all the deferred .Close() methods
	extractAndWriteFile := func(f *zip.File) error {
		rc, err := f.Open()
		if err != nil {
			return err
		}
		defer func() {
			if err := rc.Close(); err != nil {
				panic(err)
			}
		}()

		path := filepath.Join(dest, f.Name)

		if f.FileInfo().IsDir() {
			os.MkdirAll(path, f.Mode())
		} else {
			os.MkdirAll(filepath.Dir(path), f.Mode())
			f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
			if err != nil {
				return err
			}
			defer func() {
				if err := f.Close(); err != nil {
					panic(err)
				}
			}()

			_, err = io.Copy(f, rc)
			if err != nil {
				return err
			}
		}
		return nil
	}

	for _, f := range r.File {
		err := extractAndWriteFile(f)
		if err != nil {
			return err
		}
	}

	return nil
}
