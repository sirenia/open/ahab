package cmd

import (
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/sirenia/open/ahab/setup"

	"github.com/davecgh/go-spew/spew"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gopkg.in/yaml.v2"
)

var (
	confFile       string
	noRequirements bool
)

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:   "ahab",
	Short: "ahab is a tool for keeping a set of services updated and running.",
	Run: func(cmd *cobra.Command, args []string) {
		var conf setup.Configuration
		confData, err := ioutil.ReadFile(confFile)
		if err != nil {
			fmt.Errorf("Failed reading file.\n")
			panic(err)
		}
		err = yaml.Unmarshal(confData, &conf)
		if err != nil {
			fmt.Errorf("Failed unmarshalling.\n")
			panic(err)
		}
		spew.Dump(conf)
		c := setup.New(&conf)
		if err := c.Run(noRequirements); err != nil {
			fmt.Printf("Could not start ahab - %v.\n", err)
			panic(err)
		}
	},
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports Persistent Flags, which, if defined here,
	// will be global for your application.

	RootCmd.PersistentFlags().StringVarP(&confFile, "config", "c", ".config.yaml", "services config file (default is .config.yaml)")
	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	//RootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")

	RootCmd.PersistentFlags().BoolVar(&noRequirements, "noreq", false, "disable check for requirements")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {

	viper.AutomaticEnv() // read in environment variables that match

}
